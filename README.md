University of Dayton

Department of Computer Science

CPS 491 - Capstone II, Spring 2022

Instructor: Dr. Phu Phung


## Capstone II Project 


# Fingerprint login


# Team members

1. Abdulrahman Shisha, Shishaa1@udayton.edu
2. Michael Bello, Bellom1@udayton.edu


# Company Mentors

Arpan Bhattacharya, Sr. Software Engineer

Synchrony

140 Wekiva Springs Road, Longwood, FL


# Project Management Information

Management board (private access): <https://trello.com/b/aqJaF2BL/project-trello>

Source code repository (private access): <https://bitbucket.org/cps491s22-team6/cps491s22-team6.bitbucket.io/src/master/>

Project homepage (public): <https://cps491s22-team6.bitbucket.io/>

## Revision History

| Date       |   Version     |  Description |
|------------|:-------------:|-------------:|
| 17/01/2022 |  0.0          | Init draft   |


# Overview

![Architecture Overview](https://user-images.githubusercontent.com/93353218/146552480-5ac50c97-1e2a-4128-b73e-da1d08cbd2be.png)

# Project Context and Scope

In this Project, we partnered with Synchrony, a premier consumer financial services company delivering customized financing programs across key industries including retail, health, auto, travel and home, along with award-winning consumer banking products. We will be working with them to create a secure, reliable, and accurate fingerprint login.
New users are expected to register their finger print multiple times which we will save in a database/AI system. Then, whenever a user puts in their finger print it would compare it with the ones saved with the associated account, if it matches then the user logs in.

# System Analysis

The user would need to run our software and login normally for the first time, after that the user information will be encrypted in our database and our software will automatically log in that device everytime unless it is in a suspecious or different IP address or location.

## High-level Requirements

* Users must be able to register their device to save a fingerprint to the database
* Users should be able to perform tasks without logging in once registered
* Administrators should be able to manage registered devices

## Use cases
![Use Case Diagram](https://user-images.githubusercontent.com/93353218/149788927-a48f0255-02b3-4478-9417-734f52b32130.png)

## Database 
The Database was implemented using mongoDB and was connected to our application through the messengerdb.js file. In the databse we plan on encrypting our information that way even if a hacker does get into the database the hacker would not be able to get any useful information out of it, we want the database to be especially secure since our program deals with a lot of saved usernames and passwords.

## Database Design
![ER Diagram](https://user-images.githubusercontent.com/93353218/149788929-a2c5f559-d436-4980-ad6a-a473d3df309b.png)

## User Interface
* Register device to our program so that our program can recognize the IP address as well as start storing the passwords
* Once a user logs in to a new website our program would ask them if they would like to save they credentials in our program
* The automatic login.

# Implementation

We have not started any code yet, however we have been getting familiar with reactjs, which is what our advisor told us would be helpful but we might do some changes as that. We also got familiar chrome extensions and how to work with them. We also set up the basics we need for this project such as a database, reposity, management board, and a homepage.


## Deployment

The application will be deployed using Heroku for the cloud hosting. We deployed from the messenger-team-6 folder in order to keep our development code separate from our deployment code.

# Impacts

The impact of a login-less system is that it provides a very convienent UX for customers. Also, since the system will not have login credentials, it removes the ability for hackers to use other users' credentials. Instead hackers would have to spoof the hardware fingerprint which is more difficult to do. We are trying to go all out and potentially make this a google extension that everyone can use securely with ease, which will definitely be of conveniece to a lot of people.

# Software Process Management


At the beginning of the project, my team met and set rough goals for each week all the way till the end of the project. We are trying our best to achieve these goals in a timely manner, however we also meet up often in case there needs to be a change in the plan we can start working on it at soon as possible.

Trello: https://trello.com/b/aqJaF2BL/project-trello

![Trello Screenshot](https://user-images.githubusercontent.com/93353218/149789798-896ba2f4-45be-4317-8f65-0d4d8e0203e6.PNG)


![grantt](https://user-images.githubusercontent.com/90535047/146551040-296d7b8c-b7c9-4c41-a7df-bb59b9c78926.PNG)


## Scrum process

### Sprint 0

Duration: 10/01/2022-17/01/2022

#### Completed Tasks: 

1. Created a Spring 0 branch
2. Extended the proposal
3. Revised the Trello Board
4. Tool/software installation
5. Updated homepage

#### Contributions: 

1.  Abdul Shisha, 12 commits, 2 hours, contributed in Readme and homepage
2.  Mike Bello, 4 commits, 2 hours, contributed in Readme and Trello


### Sprint 1

Duration: MM/DD/YYYY-MM/DD/YYYY

#### Completed Tasks: 

1. Task 1
2. Task 2
3. ...

#### Contributions: 

1.  Member 1, x commits, y hours, contributed in xxx
2.  Member 2, x commits, y hours, contributed in xxx
3.  Member 3, x commits, y hours, contributed in xxx
4.  Member 4, x commits, y hours, contributed in xxx

#### Sprint Retrospection:

_(Introduction to Sprint Retrospection:

_Working through the sprints is a continuous improvement process. Discussing the sprint has just completed can improve the next sprints walk through a much efficient one. Sprint retrospection is done once a sprint is finished and the team is ready to start another sprint planning meeting. This discussion can take up to 1 hour depending on the ideal team size of 6 members. 
Discussing good things happened during the sprint can improve the team's morale, good team-collaboration, appreciating someone who did a fantastic job to solve a blocker issue, work well-organized, helping someone in need. This is to improve the team's confidence and keep them motivated.
As a team, we can discuss what has gone wrong during the sprint and come-up with improvement points for the next sprints. Few points can be like, need to manage time well, need to prioritize the tasks properly and finish a task in time, incorrect design lead to multiple reviews and that wasted time during the sprint, team meetings were too long which consumed most of the effective work hours. We can mention every problem is in the sprint which is hindering the progress.
Finally, this meeting should improve your next sprint drastically and understand the team dynamics well. Mention the bullet points and discuss how to solve it.)_

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|          |                             |                   |


### Sprint x

Duration: MM/DD/YYYY-MM/DD/YYYY

#### Completed Tasks: 

1. Task 1
2. Task 2
3. ...

#### Contributions: 

1.  Member 1, x commits, y hours, contributed in xxx
2.  Member 2, x commits, y hours, contributed in xxx
3.  Member 3, x commits, y hours, contributed in xxx
4.  Member 4, x commits, y hours, contributed in xxx

#### Sprint Retrospection: 

Good: We met on time and early on in the project, we have most of our goals set
Could have been better: I think we will need to go over our deadlines and make sure that everything can be done so that we will not fall behind
How to improve: Revise the plan and make sure it is all good, and come up with a plan B



# User guide/Demo

The user would need to run our software and login normally for the first time, after that the user information will be encrypted in our database and our software will automatically log in that device everytime unless it is in a suspecious or different IP address or location.


# Acknowledgments 

We would like to thank Synchrony for teaming up with us and giving us the chance to work on this amazing opportunity.